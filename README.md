# Trainee bot : Le stagiaire !
Petit bot qui doit au moins servir le café.

[![Build Status](https://drone.beedi.xyz/api/badges/mad/traineeBot/status.svg)](https://drone.beedi.xyz/mad/traineeBot)

# Liens :
## Apis
* Kaamelott : https://github.com/sin0light/api-kaamelott/

## Autres
* Discord4j : https://github.com/Discord4J/Discord4J
* Http.Cat : https://http.cat/

