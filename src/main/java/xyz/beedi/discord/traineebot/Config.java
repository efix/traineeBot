package xyz.beedi.discord.traineebot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;

@Configuration
@EnableScheduling
public class Config {

	@Bean
	public GatewayDiscordClient discordClient(@Value("${discord.bot.token}") String token) {
		return  DiscordClientBuilder
				.create(token).build().login().block();
	}
	
}
