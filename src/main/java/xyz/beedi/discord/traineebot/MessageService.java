package xyz.beedi.discord.traineebot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.rest.util.Color;

@Service
public class MessageService {

  private static final Logger LOGGER = LoggerFactory.getLogger("MessageService");

  @Autowired private GatewayDiscordClient client;

  public void sendEmbeddedToAllSystemChannels(String message) {
    sendEmbeddedToAllSystemChannels(message, Color.RED, "Super info !");
    LOGGER.info("Dispating message to all guilds ...");
  }

  public void sendEmbeddedToAllSystemChannels(String message, Color color, String title) {
    LOGGER.info("Dispating message to all guilds ...");
    client
        .getGuilds()
        .toStream()
        .forEach(
            guild -> {
              TextChannel channel = guild.getSystemChannel().block();
              LOGGER.info("Sending to channel : {}-#{}", guild.getName(), channel.getName());
              channel
                  .createEmbed(spec -> spec.setColor(color).setTitle(title).setDescription(message))
                  .subscribe();
            });
  }

  public void sendToAllSystemChannels(String message) {
    LOGGER.info("Dispating message to all guilds ...");
    client
        .getGuilds()
        .toStream()
        .forEach(
            guild -> {
              TextChannel channel = guild.getSystemChannel().block();
              LOGGER.info("Sending to channel : {}-#{}", guild.getName(), channel.getName());
              channel.createMessage(message).subscribe();
            });
  }
}
