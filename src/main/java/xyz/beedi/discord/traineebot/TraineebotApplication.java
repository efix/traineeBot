package xyz.beedi.discord.traineebot;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.ReactiveEventAdapter;
import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.object.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import reactor.core.publisher.Mono;
import xyz.beedi.discord.traineebot.commands.HttpCatCommand;
import xyz.beedi.discord.traineebot.commands.KaamelottCommands;
import xyz.beedi.discord.traineebot.commands.PloufPloufCommand;
import xyz.beedi.discord.traineebot.commands.ServiceCommand;
import xyz.beedi.discord.traineebot.events.*;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class TraineebotApplication {

  private final GatewayDiscordClient client;
  private final CoffeeEvents coffeeEvents;
  private final FunEvents funEvents;
  private final TechnicalEvents technicalEvents;
  private final OrdresEvents ordresEvents;
  private final KaamelottEvents kaamelottEvents;
  private final PolitesseEvents politesseEvents;
  private final HttpCatEvents httpCatEvents;
  private final KaamelottCommands kaamelottCommands;
  private final ServiceCommand serviceCommand;
  private final HttpCatCommand httpCatCommand;
  private final PloufPloufCommand ploufPloufCommand;

  public static void main(String[] args) {
    SpringApplication.run(TraineebotApplication.class, args);
  }

  @EventListener(ApplicationReadyEvent.class)
  public void onReady() {

    client
        .getEventDispatcher()
        .on(ReadyEvent.class)
        .subscribe(
            event -> {
              final User self = event.getSelf();
              log.info("Logged in as {}#{}", self.getUsername(), self.getDiscriminator());
            });

    log.info("Coucou je suis là");

    technicalEvents.registerTechnicalEvents();
    funEvents.registerFunEvents();
    coffeeEvents.registerCoffeeEvents();
    ordresEvents.obeisAuxOrdres();
    kaamelottEvents.registerKaamelottEvents();
    politesseEvents.soisPoli();
    httpCatEvents.registerCats();

    registerCommands();

    client.onDisconnect().block();

    log.info("Good bye");
  }

  private void registerCommands() {
    kaamelottCommands.registerCommand();
    httpCatCommand.registerCommand();
    serviceCommand.registerCommands();
    ploufPloufCommand.registerCommand();

    client
        .on(
            new ReactiveEventAdapter() {

              @Override
              public Publisher<?> onSlashCommand(SlashCommandEvent commandEvent) {
                log.debug("Command" + commandEvent);

                switch (commandEvent.getCommandName()) {
                  case KaamelottCommands.COMMAND_NAME:
                    return kaamelottCommands.onCommand(commandEvent);
                  case ServiceCommand.COMMAND_NAME_SERVICE:
                  case ServiceCommand.COMMAND_NAME_CAFE:
                  case ServiceCommand.COMMAND_NAME_THE:
                  case ServiceCommand.COMMAND_NAME_SOIF:
                    return serviceCommand.onCommand(commandEvent);
                  case HttpCatCommand.COMMAND_NAME:
                    return httpCatCommand.onCommand(commandEvent);
                  case PloufPloufCommand.COMMAND_NAME:
                    return ploufPloufCommand.onCommand(commandEvent);

                  default:
                    log.error("Commande inconnue : " + commandEvent.getCommandName());
                    break;
                }

                return Mono.empty();
              }
            })
        .blockLast();
  }
}
