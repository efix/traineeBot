package xyz.beedi.discord.traineebot.commands;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.discordjson.json.ApplicationCommandRequest;
import discord4j.rest.RestClient;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

@Slf4j
public abstract class AbstractCommand {

  @Autowired private GatewayDiscordClient client;

  /** @return Command's name */
  abstract String commandName();

  /** @return Command's description */
  abstract String description();

  /** @return Command's options */
  List<ApplicationCommandOptionData> options() {
    return Collections.emptyList();
  }

  abstract Publisher<?> onCommand(SlashCommandEvent commandEvent);

  public void registerCommand() {
    ApplicationCommandRequest command =
        ApplicationCommandRequest.builder()
            .name(commandName())
            .description(description())
            .addAllOptions(options())
            .build();

    registerCommand(command);
  }

  void registerCommand(ApplicationCommandRequest command) {
    RestClient restClient = client.getRestClient();

    long applicationId = restClient.getApplicationId().block();

    restClient
        .getApplicationService()
        .createGlobalApplicationCommand(applicationId, command)
        .doOnError(e -> log.warn("Impossible de créer la commande [{}]", command.name(), e))
        .onErrorResume(e -> Mono.empty())
        .block();

    log.info("Commande [{}] prête !", command.name());
  }
}
