package xyz.beedi.discord.traineebot.commands;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.rest.util.ApplicationCommandOptionType;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import xyz.beedi.discord.traineebot.services.HttpCatService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class HttpCatCommand extends AbstractCommand {

  public static final String COMMAND_NAME = "httpcat";
  private static final String OPTION_STATUS = "code";
  @Autowired private GatewayDiscordClient client;
  @Autowired private HttpCatService httpCatService;

  @Override
  String commandName() {
    return COMMAND_NAME;
  }

  @Override
  String description() {
    return "Demande à un chat.";
  }

  @Override
  List<ApplicationCommandOptionData> options() {
    return Arrays.asList(
        ApplicationCommandOptionData.builder()
            .name(OPTION_STATUS)
            .description("Http Code")
            .type(ApplicationCommandOptionType.INTEGER.getValue())
            .required(true)
            .build());
  }

  @Override
  public Publisher<?> onCommand(SlashCommandEvent commandEvent) {
    HttpStatus status =
        HttpStatus.resolve(
            (int) commandEvent.getOption(OPTION_STATUS).get().getValue().get().asLong());
    if (status != null) {
      httpCatService
          .afficheHttpCat(status, commandEvent.getInteraction().getChannel().block())
          .subscribe();
    } else {
      commandEvent
          .getInteraction()
          .getChannel()
          .block()
          .createMessage(
              "J'ai beau chercher je ne trouver pas le status Http \""
                  + commandEvent.getOption(OPTION_STATUS).get().getValue().get().getRaw()
                  + "\"\n"
                  + "Ceux que je connais presque par coeur :"
                  + "\n -"
                  + Arrays.asList(HttpStatus.values()).stream()
                      .sorted(Comparator.comparing(HttpStatus::value))
                      .map(s -> s.value() + " : " + s.getReasonPhrase())
                      .collect(Collectors.joining("\n - ")))
          .subscribe();
    }
    return commandEvent.replyEphemeral("Miaouuu");
  }
}
