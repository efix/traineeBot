package xyz.beedi.discord.traineebot.commands;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.rest.util.ApplicationCommandOptionType;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottPersonnage;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottServices;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class KaamelottCommands extends AbstractCommand {

  public static final String COMMAND_NAME = "kaamelott";
  private static final String OPTION_PERSONNAGE = "personnage";
  @Autowired private GatewayDiscordClient client;
  @Autowired private KaamelottServices kaamelottServices;

  @Override
  String commandName() {
    return COMMAND_NAME;
  }

  @Override
  String description() {
    return "Lance une citation de Kaamelott";
  }

  @Override
  List<ApplicationCommandOptionData> options() {
    return Arrays.asList(
        ApplicationCommandOptionData.builder()
            .name(OPTION_PERSONNAGE)
            .description("Personnage")
            .type(ApplicationCommandOptionType.STRING.getValue())
            .addAllChoices(getKaamelottChoices())
            .required(false)
            .build());
  }

  private List<ApplicationCommandOptionChoiceData> getKaamelottChoices() {
    return Arrays.asList(KaamelottPersonnage.values()).stream()
        .sorted()
        .map(
            p ->
                ApplicationCommandOptionChoiceData.builder()
                    .name(p.getLabel())
                    .value(p.name())
                    .build())
        .collect(Collectors.toList());
  }

  private Mono<Void> listerPersos(SlashCommandEvent event) {
    return event.replyEphemeral(
        "Liste des personnage dispos : \n"
            + Stream.of(KaamelottPersonnage.values())
                .map(KaamelottPersonnage::getLabel)
                .sorted()
                .collect(Collectors.joining(",\n")));
  }

  @Override
  public Publisher<?> onCommand(SlashCommandEvent event) {
    if (event.getOption(OPTION_PERSONNAGE).isPresent()) {
      KaamelottPersonnage perso =
          KaamelottPersonnage.find(
              event.getOption(OPTION_PERSONNAGE).get().getValue().get().asString());
      if (perso != null) {
        kaamelottServices
            .citerUnPersonnage(
                perso,
                (MessageChannel)
                    client.getChannelById(event.getInteraction().getChannelId()).block())
            .subscribe();
        return event.replyEphemeral("C'est darty !");
      } else {
        return listerPersos(event);
      }

    } else {
      kaamelottServices
          .citerUnPersonnageAuPif(
              (MessageChannel) client.getChannelById(event.getInteraction().getChannelId()).block())
          .subscribe();
      return event.replyEphemeral("C'est darty !");
    }
  }
}
