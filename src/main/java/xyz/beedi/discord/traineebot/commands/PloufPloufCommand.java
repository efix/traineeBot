package xyz.beedi.discord.traineebot.commands;


import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.presence.Status;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@SuppressWarnings("BlockingMethodInNonBlockingContext")
@Slf4j
@Service
public class PloufPloufCommand extends AbstractCommand {

  public static final String COMMAND_NAME = "plouf-plouf";

  @Override
  String commandName() {
    return COMMAND_NAME;
  }

  @Override
  String description() {
    return "Je choisi qui va perdre";
  }

  @Override
  public Publisher<?> onCommand(SlashCommandEvent commandEvent) {
    MessageChannel channel = commandEvent.getInteraction().getChannel().block();
    if (channel instanceof TextChannel) {

      List<Member> members =
          ((TextChannel) channel)
              .getMembers()
              .filter(member -> !member.isBot())
              .collectList()
              .block();

      Member perdant = members.get(new Random().nextInt(members.size()));

      channel.createMessage("Plouf plouf, ca sera toi qui perdera").subscribe();
      channel.createMessage("\uD83E\uDD41").subscribe();
      new Thread(
              () -> {
                try {
                  long attente = new Random().nextInt(20) * 1_000;
                  if (attente > 5_000) {
                    new Thread(
                            () -> {
                              try {
                                Thread.sleep(5_000);
                              } catch (InterruptedException e) {

                              }
                              channel.createMessage("(Quel suspens !!)").subscribe();
                            })
                        .start();
                  }
                  Thread.sleep(attente);
                } catch (InterruptedException e) {
                  log.error("Je peux plus attendre !", e);
                }
                channel.createMessage(perdant.getMention() + " t'as perduuuu").subscribe();

                if (perdant.getPresence().block().getStatus() == Status.OFFLINE) {
                  channel.createMessage("Les absents ont toujours tort").subscribe();
                }
              })
          .start();
    } else {
      return commandEvent.replyEphemeral("Perdu");
    }

    return commandEvent.replyEphemeral("Attention je vais tirer ! (au sort)");
  }
}
