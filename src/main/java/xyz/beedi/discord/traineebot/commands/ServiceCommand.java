package xyz.beedi.discord.traineebot.commands;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.interaction.SlashCommandEvent;
import discord4j.core.object.entity.User;
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData;
import discord4j.discordjson.json.ApplicationCommandOptionData;
import discord4j.discordjson.json.ApplicationCommandRequest;
import discord4j.discordjson.json.ImmutableApplicationCommandOptionData;
import discord4j.rest.util.ApplicationCommandOptionType;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import xyz.beedi.discord.traineebot.services.CoffeeService;
import xyz.beedi.discord.traineebot.services.ServiceTypes;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ServiceCommand extends AbstractCommand {

  @Autowired private GatewayDiscordClient client;
  @Autowired private CoffeeService coffeeService;

  public static final String COMMAND_NAME_SERVICE = "service";
  public static final String COMMAND_NAME_CAFE = "café";
  public static final String COMMAND_NAME_THE = "thé";
  public static final String COMMAND_NAME_SOIF = "soif";
  private static final String OPTION_SERVICE_TYPE = "quoi";
  private static final String OPTION_POUR_QUI = "pour";

  @Override
  String commandName() {
    // Keep empty won't use "basic one"
    return null;
  }

  @Override
  String description() {
    // Keep empty won't use "basic one"
    return null;
  }

  @Override
  List<ApplicationCommandOptionData> options() {
    // Keep empty won't use "basic one"
    return null;
  }

  public void registerCommands() {
    registerMainCommand();
    registerCaféCommand();
    registerThéCommand();
    registerSoifCommand();
  }

  private void registerMainCommand() {
    registerCommand(
        ApplicationCommandRequest.builder()
            .name(COMMAND_NAME_SERVICE)
            .description("Je vous sers un truc à boire ou à manger")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name(OPTION_SERVICE_TYPE)
                    .description("Quel truc servir ?")
                    .type(ApplicationCommandOptionType.STRING.getValue())
                    .addAllChoices(getServiceTypesOptions())
                    .required(true)
                    .build())
            .addOption(buildPourQui())
            .build());
  }

  private void registerCaféCommand() {
    registerCommand(
        ApplicationCommandRequest.builder()
            .name(COMMAND_NAME_CAFE)
            .description("Je vous sers un café")
            .addOption(buildPourQui())
            .build());
  }

  private void registerThéCommand() {
    registerCommand(
        ApplicationCommandRequest.builder()
            .name(COMMAND_NAME_THE)
            .description("Je vous sers un thé")
            .addOption(buildPourQui())
            .build());
  }

  private void registerSoifCommand() {
    registerCommand(
        ApplicationCommandRequest.builder()
            .name(COMMAND_NAME_SOIF)
            .description("Je vous sers un petit \"rafraîchissement\"")
            .addOption(buildPourQui())
            .build());
  }


  private ImmutableApplicationCommandOptionData buildPourQui() {
    return ApplicationCommandOptionData.builder()
        .name(OPTION_POUR_QUI)
        .description("Servir à qui ?")
        .type(ApplicationCommandOptionType.USER.getValue())
        .required(false)
        .build();
  }

  private List<ApplicationCommandOptionChoiceData> getServiceTypesOptions() {
    return Arrays.asList(ServiceTypes.values())
        .stream()
        .sorted()
        .map(
            t ->
                ApplicationCommandOptionChoiceData.builder()
                    .name(StringUtils.capitalize(t.getLabel()))
                    .value(t.name())
                    .build())
        .collect(Collectors.toList());
  }

  public Publisher<?> onCommand(SlashCommandEvent commandEvent) {
    ServiceTypes type;
    switch (commandEvent.getCommandName()) {
      case COMMAND_NAME_SERVICE:
        type =
            ServiceTypes.valueOf(
                commandEvent.getOption(OPTION_SERVICE_TYPE).get().getValue().get().asString());
        break;
      case COMMAND_NAME_CAFE:
        type = ServiceTypes.CAFÉ;
        break;
      case COMMAND_NAME_THE:
        type = ServiceTypes.THÉ;
        break;
      case COMMAND_NAME_SOIF:
        type = ServiceTypes.RAFRAICHISSEMENT;
        break;
      default:
        throw new IllegalStateException("Unsupported Command");
    }

    User user = commandEvent.getInteraction().getUser();

    if (commandEvent.getOption(OPTION_POUR_QUI).isPresent()) {
      user = commandEvent.getOption(OPTION_POUR_QUI).get().getValue().get().asUser().block();
    }

    coffeeService
        .sersUnTruc(type, commandEvent.getInteraction().getChannel().block(), user)
        .subscribe();

    return commandEvent.replyEphemeral("Oui boss !");
  }
}
