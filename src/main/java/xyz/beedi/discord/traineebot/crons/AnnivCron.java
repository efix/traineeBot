package xyz.beedi.discord.traineebot.crons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import discord4j.rest.util.Color;
import xyz.beedi.discord.traineebot.MessageService;

@Service
public class AnnivCron {
  @Autowired MessageService messageService;

  @Scheduled(cron = "0 0 7 5 7 ?")
  public void annivDuCreateur() {
    messageService.sendEmbeddedToAllSystemChannels(
        "Et on souhaite un bon anniversaire à mon maître de stage et créateur pour ses 11èmes 25 ans 🎁",
        Color.MAGENTA,
        "🎂");
  }
}
