package xyz.beedi.discord.traineebot.crons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import discord4j.rest.util.Color;
import xyz.beedi.discord.traineebot.MessageService;

@Service
public class TempoCrons {

  @Autowired MessageService messageService;

  @Scheduled(cron = "0 0 16 * * MON-THU")
  public void tempoCron1() {
    messageService.sendEmbeddedToAllSystemChannels("Et on n'oublie pas son tempo", Color.RED, "Rappel");
  }

  @Scheduled(cron = "0 30 11 * * FRI")
  public void tempoCron2() {
    messageService.sendEmbeddedToAllSystemChannels(
        "Et on oublie pas son tempo, même si c'est vendredi", Color.RED, "Rappel");
  }
}
