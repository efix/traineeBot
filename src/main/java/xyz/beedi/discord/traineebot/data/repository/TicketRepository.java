package xyz.beedi.discord.traineebot.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import xyz.beedi.discord.traineebot.data.model.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Long> {

  List<Ticket> findByUser(String user);

  @Override
  List<Ticket> findAll();
}
