package xyz.beedi.discord.traineebot.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.core.object.reaction.ReactionEmoji.Unicode;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import xyz.beedi.discord.traineebot.services.CoffeeService;
import xyz.beedi.discord.traineebot.services.ServiceTypes;
import xyz.beedi.discord.traineebot.utils.BotUtils;

@Service
@Slf4j
public class CoffeeEvents {

  public static final List<String> SERVICE_KEYWORDS =
      Arrays.asList("sers un", "donne un", "paye un", "sers moi un", "donne moi un", "paye moi un");

  @Autowired private GatewayDiscordClient client;
  @Autowired private CoffeeService coffeeService;

  public void registerCoffeeEvents() {
    // Propose
    registerCoffee();
    registerTea();
    registerRafraichissement();
    registerCroissants();

    // réagis aux emojis
    sersLeCafé();
    sersLeThé();
    sersLesCroissants();
    sersLesRafraichissements();

    // autres
    sersLeCafeAQuelquun();
  }

  private void registerCoffee() {
    registerUnTrucDuMatin(ServiceTypes.CAFÉ);
  }

  private void registerUnTrucDuMatin(ServiceTypes truc) {
    List<String> keywords = new ArrayList<>(truc.getKeywords());
    keywords.addAll(PolitesseEvents.BONJOUR_KEYWORDS);

    Pattern p = BotUtils.patternForKeywords(keywords);

    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> p.matcher(message.getContent()).matches())
        .filter(message -> !OrdresEvents.DIS_BONJOUR.matcher(message.getContent()).matches())
        .flatMap(message -> message.addReaction(ReactionEmoji.unicode(truc.getEmoji())))
        .subscribe();
    log.info("Prêt à proposer le " + truc.getLabel() + " !");
  }

  private void registerUnTruc(ServiceTypes truc) {
    List<String> keywords = new ArrayList<>(truc.getKeywords());

    Pattern p = BotUtils.patternForKeywords(keywords);

    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> p.matcher(message.getContent()).matches())
        .flatMap(message -> message.addReaction(ReactionEmoji.unicode(truc.getEmoji())))
        .subscribe();
    log.info("Prêt à proposer le " + truc.getLabel() + " !");
  }

  private void registerCroissants() {
    registerUnTrucDuMatin(ServiceTypes.CROISSANT);
  }

  private void registerTea() {
    registerUnTrucDuMatin(ServiceTypes.THÉ);
  }

  private void registerRafraichissement() {
    registerUnTruc(ServiceTypes.RAFRAICHISSEMENT);
  }

  private void sersUnTrucSurEmoji(
      ServiceTypes truc, BiFunction<MessageChannel, User, Mono<? extends Message>> serviceMethod) {
    client
        .getEventDispatcher()
        .on(ReactionAddEvent.class)
        .filter(
            event ->
                event
                    .getEmoji()
                    .asUnicodeEmoji()
                    .map(Unicode::getRaw)
                    .orElse("")
                    .equals(truc.getEmoji()))
        .filter(event -> event.getMember().isPresent() && !event.getMember().get().isBot())
        .flatMap(
            event -> {
              MessageChannel channel = event.getChannel().block();

              return serviceMethod.apply(channel, event.getMember().get());
            })
        .subscribe();
    log.info("Prêt à servir le café !");
  }

  private void sersLeCafé() {
    sersUnTrucSurEmoji(ServiceTypes.CAFÉ, coffeeService::sersUnCafe);
  }

  private void sersLesCroissants() {
    sersUnTrucSurEmoji(ServiceTypes.CROISSANT, coffeeService::sersUnCroissant);
  }

  private void sersLeThé() {
    sersUnTrucSurEmoji(ServiceTypes.THÉ, coffeeService::sersUnThe);
  }

  private void sersLesRafraichissements() {
    sersUnTrucSurEmoji(ServiceTypes.RAFRAICHISSEMENT, coffeeService::sersUnRafraichissement);
  }

  private void sersLeCafeAQuelquun() {
    Pattern pCoffee = BotUtils.patternForKeywords(ServiceTypes.CAFÉ.getKeywords());

    Pattern pService = BotUtils.patternForKeywords(SERVICE_KEYWORDS);

    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> pService.matcher(message.getContent()).matches())
        .filter(message -> pCoffee.matcher(message.getContent()).matches())
        .doOnEach(
            messageSignal -> {
              MessageChannel channel = messageSignal.get().getChannel().block();
              if (messageSignal.get().getUserMentionIds().isEmpty()
                  || (messageSignal.get().getUserMentionIds().size() == 1
                      && messageSignal.get().getUserMentionIds().contains(client.getSelfId()))) {
                // set un café à celui qui cause
                messageSignal
                    .get()
                    .getAuthor()
                    .ifPresent(user -> coffeeService.sersUnCafe(channel, user).subscribe());
              } else {
                messageSignal
                    .get()
                    .getUserMentionIds()
                    .stream()
                    .map(snow -> client.getUserById(snow).block())
                    .filter(Predicate.not(User::isBot))
                    .forEach(user -> coffeeService.sersUnCafe(channel, user).subscribe());
              }
            })
        .subscribe();
    log.info("Prêt à servir le café si on me demande bien!");
  }
}
