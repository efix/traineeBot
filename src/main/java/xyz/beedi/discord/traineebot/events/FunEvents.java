package xyz.beedi.discord.traineebot.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.Channel.Type;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottPersonnage;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottServices;
import xyz.beedi.discord.traineebot.utils.BotUtils;

@Service
@Slf4j
public class FunEvents {

  private static final List<String> CHUT_MESSAGES =
      Arrays.asList(
          "Chut 🤫",
          "Un peu de silence y'en a qui bossent ici !",
          "Je n'ai pas de déclaration à faire",
          "Y'en a qui n'ont vraiment rien de mieux à faire ...");

  private static final List<String> WIII_REPLIQUES =
      Arrays.asList(
          "Oui messire ?",
          "Qui y a-t-il ?",
          "Encore du travail ?",
          "Pardon ?",
          "Oui monseigneur !",
          "Votre prénom c'est François, c'est juste ?",
          "Il essaie de faire de l'humour. Écoutez ça, c'est pathétique.",
          "Il pourrait me faire l'historique de la louche à travers les âges.",
          "Il est bon !? C'est un champion du monde !");

  private static final List<String> OTHER_REPLIQUES =
      Arrays.asList(
          // PH
          "C'est inadmettable !",
          "T'es tout seul dans ton slip pour me dire ça ?",
          "Faudrait pas risquer le claquage",
          "Espèce de porc !",
          "C'est pas ma guerre !",
          "J'disperse, j'ventile façon puzzle");

  private static final List<String> BUG_KEYWORDS = Arrays.asList("bug", "malisse", "jpm");

  private static final List<String> BUG_REPONSES =
      Arrays.asList("Y'a un démon dans la synchronisation !", "This is unacceptable !");

  private static final List<String> ALL_REPLIQUES = new ArrayList<>();

  static {
    ALL_REPLIQUES.addAll(WIII_REPLIQUES);
    ALL_REPLIQUES.addAll(OTHER_REPLIQUES);
  }

  @Autowired private GatewayDiscordClient client;
  @Autowired private KaamelottServices kaamelottServices;

  public void registerFunEvents() {
    registerChutMP();
    registerPingPong();
    registerMentions();
    registerBugs();
  }

  private void registerChutMP() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getChannel().block().getType() == Type.DM)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .flatMap(Message::getChannel)
        .flatMap(
            channel ->
                channel.createMessage(
                    CHUT_MESSAGES.get(new Random().nextInt(CHUT_MESSAGES.size()))))
        .subscribe();

    log.info("Prêt à travailler !");
  }

  private void registerMentions() {
    Pattern serviceKeywords = BotUtils.patternForKeywords(CoffeeEvents.SERVICE_KEYWORDS);
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(client.getSelfId()))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> !OrdresEvents.DIS_BONJOUR.matcher(message.getContent()).matches())
        .filter(message -> !OrdresEvents.PRESENTE_TOI.matcher(message.getContent()).matches())
        .filter(message -> !OrdresEvents.EXCUSE_TOI.matcher(message.getContent()).matches())
        .filter(message -> !serviceKeywords.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(
            channel ->
                channel.createMessage(
                    ALL_REPLIQUES.get(new Random().nextInt(ALL_REPLIQUES.size()))))
        .subscribe();

    log.info("Prêt à répliquer !");
  }

  private void registerPingPong() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> message.getContent().equalsIgnoreCase("ping"))
        .flatMap(
            message -> {
              message.addReaction(ReactionEmoji.unicode("🏓")).subscribe();
              MessageChannel channel = message.getChannel().block();
              Snowflake ref = message.getId();
              return channel.createMessage(
                  spec -> spec.setMessageReference(ref).setContent("Pong !"));
            })
        .subscribe();

    log.info("Prêt à jouer au Ping-Pong !");
  }

  private void registerBugs() {
    Pattern p = BotUtils.patternForKeywords(BUG_KEYWORDS);
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> p.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(
            channel -> {
              if (new Random().nextBoolean()) {
                return channel.createMessage(
                    BUG_REPONSES.get(new Random().nextInt(BUG_REPONSES.size())));
              } else {
                return kaamelottServices.citerUnPersonnage(KaamelottPersonnage.KADOC, channel);
              }
            })
        .subscribe();

    log.info("Prêt à trouver des démons !");
  }
}
