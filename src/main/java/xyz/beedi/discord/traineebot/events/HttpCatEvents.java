package xyz.beedi.discord.traineebot.events;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.services.HttpCatService;
import xyz.beedi.discord.traineebot.utils.BotUtils;

@Slf4j
@Service
public class HttpCatEvents {

  @Autowired private GatewayDiscordClient client;
  @Autowired private HttpCatService httpCatService;

  public void registerCats() {
    Pattern p =
        BotUtils.patternForKeywords(
            Arrays.asList(HttpStatus.values())
                .stream()
                .map(s -> Integer.toString(s.value()))
                .collect(Collectors.toList()));
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> p.matcher(message.getContent()).matches())
        .flatMap(
            message -> {
              Matcher m = p.matcher(message.getContent());
              m.find();
              String httpStr = m.group(2);
              HttpStatus status = HttpStatus.resolve(Integer.valueOf(httpStr));
              return httpCatService.afficheHttpCat(status, message.getChannel().block());
            })
        .subscribe();

    log.info("Prêt à trouver des chats !");
  }
}
