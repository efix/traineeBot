package xyz.beedi.discord.traineebot.events;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottPersonnage;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottServices;

@Service
@Slf4j
public class KaamelottEvents {

  public static final Pattern CITE_AU_PIF =
      Pattern.compile(
          ".*(kaamelott|kamelot|kamelott|kaamelot|ennui|kmlt).*", Pattern.CASE_INSENSITIVE);

  @Autowired private GatewayDiscordClient client;
  @Autowired private KaamelottServices kaamelottServices;

  public void registerKaamelottEvents() {
    citerKaamelottAuPif();
    citerUnPersonnage();
  }

  private void citerKaamelottAuPif() {

    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> CITE_AU_PIF.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(kaamelottServices::citerUnPersonnageAuPif)
        .subscribe();

    log.info("Prêt à citer Kaamelott au pif !");
  }

  private void citerUnPersonnage() {
    for (KaamelottPersonnage personnage : KaamelottPersonnage.values()) {
      client
          .getEventDispatcher()
          .on(MessageCreateEvent.class)
          .map(MessageCreateEvent::getMessage)
          .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
          .filter(message -> personnage.getPattern().matcher(message.getContent()).matches())
          .flatMap(Message::getChannel)
          .flatMap(channel -> kaamelottServices.citerUnPersonnage(personnage, channel))
          .subscribe();

      log.info("Prêt à citer {} !", personnage);
    }
  }
}
