package xyz.beedi.discord.traineebot.events;

import java.util.function.Function;
import java.util.regex.Pattern;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.common.util.Snowflake;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.util.Color;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.data.model.Ticket;
import xyz.beedi.discord.traineebot.services.TicketService;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottServices;

@Service
@Slf4j
public class OrdresEvents {

  public static final Pattern PRESENTE_TOI =
      Pattern.compile(".*(présente toi).*", Pattern.CASE_INSENSITIVE);

  public static final Pattern DIS_BONJOUR =
      Pattern.compile(".*(dis bonjour).*", Pattern.CASE_INSENSITIVE);

  public static final Pattern DIS_AU_REVOIR =
      Pattern.compile(".*(dis bonsoir|dis aurevoir).*", Pattern.CASE_INSENSITIVE);

  public static final Pattern EXCUSE_TOI =
      Pattern.compile(".*(dis pardon|excuse toi).*", Pattern.CASE_INSENSITIVE);

  @Autowired private GatewayDiscordClient client;
  @Autowired private TicketService ticketService;
  @Autowired private KaamelottServices kaamelottServices;

  public void obeisAuxOrdres() {
    registerPresenteToi();
    registerDisBonjour();
    registerDisPardon();
    registerDisAuRevoir();
    prendsLesTickets();
  }

  private void prendsLesTickets() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(Snowflake.of("689033285891915782")))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> !message.getReferencedMessage().isPresent())
        .flatMap(
            message -> {
              Ticket ticket =
                  ticketService.createTicket(
                      message.getAuthor().get().getId(), message.getChannelId());

              message
                  .getChannel()
                  .block()
                  .createMessage(
                      "Voilà un ticket pour patienter. Vous avez avez le ticket #" + ticket.getId())
                  .subscribe();
              return kaamelottServices.citerUnPersonnageAuPif(message.getChannel().block());
            })
        .subscribe();

    log.info("Prêt à donner des tickets !");
  }

  private void registerPresenteToi() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(client.getSelfId()))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> OrdresEvents.PRESENTE_TOI.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(presenteToi())
        .subscribe();

    log.info("Prêt à me présenter !");
  }

  private void registerDisBonjour() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(client.getSelfId()))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> OrdresEvents.DIS_BONJOUR.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(disBonjour())
        .subscribe();

    log.info("Prêt à dire bonjour !");
  }

  private Function<? super MessageChannel, ? extends Publisher<? extends Message>> presenteToi() {
    return channel ->
        channel.createEmbed(
            spec ->
                spec.setColor(Color.BLUE)
                    .setTitle("Helloooo")
                    .setDescription(
                        "Salut salut\n"
                            + "C'est moi le nouveau stagiaire 😀\n"
                            + "Je sais faire le café et jouer au Ping Pong 🏓\n"
                            + "Et on m'a demandé de vous rappeler certains trucs aussi 😅"));
  }

  private Function<? super MessageChannel, ? extends Publisher<? extends Message>> disBonjour() {
    return channel -> channel.createMessage("Bonjour tout le monde ! 👋");
  }

  private Function<? super MessageChannel, ? extends Publisher<? extends Message>> disAuRevoir() {
    return channel -> channel.createMessage("Au revoir tout le monde ! 👋");
  }

  private void registerDisAuRevoir() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(client.getSelfId()))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> OrdresEvents.DIS_AU_REVOIR.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(disAuRevoir())
        .subscribe();

    log.info("Prêt à dire bonjour !");
  }

  private void registerDisPardon() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getUserMentionIds().contains(client.getSelfId()))
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> OrdresEvents.EXCUSE_TOI.matcher(message.getContent()).matches())
        .flatMap(Message::getChannel)
        .flatMap(disPardon())
        .subscribe();

    log.info("Prêt à dire pardon !");
  }

  private Function<? super MessageChannel, ? extends Publisher<? extends Message>> disPardon() {
    return channel -> channel.createMessage("Pardon 🥺");
  }
}
