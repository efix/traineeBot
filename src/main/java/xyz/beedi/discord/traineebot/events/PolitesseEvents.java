package xyz.beedi.discord.traineebot.events;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.reaction.ReactionEmoji;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.MessageService;
import xyz.beedi.discord.traineebot.utils.BotUtils;

@Service
@Slf4j
public class PolitesseEvents {

  @Autowired private GatewayDiscordClient client;
  @Autowired private MessageService messageService;

  public static final List<String> BONJOUR_KEYWORDS =
      Arrays.asList("bonjour", "bjr", "hello", "salut", "slt", "yo", "bom\\s+dia");

  public static final List<String> BYE_KEYWORDS =
      Arrays.asList("au revoir", "à demain", "a demain", "bye", "bon weekend", "bonne soirée");

  public static final List<String> BONJOUR_EXPRESSIONS =
      Arrays.asList(
          "Bonjour !", "Bonjour à tous !", "Salut tout le monde !", "Bon matin !", "Hello !");

  public void soisPoli() {
    disAuRevoir();
  }

  private void disAuRevoir() {
    Pattern p = BotUtils.patternForKeywords(BYE_KEYWORDS);

    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(message -> message.getAuthor().map(user -> !user.isBot()).orElse(false))
        .filter(message -> p.matcher(message.getContent()).matches())
        .flatMap(
            message -> {
              if (new Random().nextBoolean()) {
                return message.getChannel().block().createMessage("Ciao les naz !");
              }
              return message.addReaction(ReactionEmoji.unicode("👋"));
            })
        .subscribe();

    log.info("Prêt à dire au revoir");
  }

  private void disBonjour() {
    messageService.sendToAllSystemChannels(
        BONJOUR_EXPRESSIONS.get(new Random().nextInt(BONJOUR_EXPRESSIONS.size())));
  }

  @Scheduled(cron = "0 0 7 * * MON-FRI")
  public void disBonjourLeMatin() {
    disBonjour();
    if (new Random().nextInt(10) == 5) {
      // une chance sur 10 de payer les croissants :)
      payeTonCroissants();
    }
  }

  private void payeTonCroissants() {
    messageService.sendToAllSystemChannels(
        "Vous êtes tellement cool avec moi, je paye ma tournée de croissants !! 🥐");
  }
}
