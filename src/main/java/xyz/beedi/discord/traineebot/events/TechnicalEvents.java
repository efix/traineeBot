package xyz.beedi.discord.traineebot.events;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.channel.MessageChannel;
import lombok.extern.slf4j.Slf4j;
import xyz.beedi.discord.traineebot.data.model.Ticket;
import xyz.beedi.discord.traineebot.services.TicketService;

@Service
@Slf4j
public class TechnicalEvents {

  @Autowired private GatewayDiscordClient client;
  @Autowired private TicketService ticketService;

  public void registerTechnicalEvents() {
    registerLogs();
    listTickets();
  }

  private void listTickets() {
    List<Ticket> tickets = ticketService.getAllTickets();
    if (tickets != null) {
      log.info("{} tickets en attente", tickets.size());
      log.info(
          String.join("\n", tickets.stream().map(Ticket::toString).collect(Collectors.toList())));
    }
  }

  private void registerLogs() {
    client
        .getEventDispatcher()
        .on(MessageCreateEvent.class)
        .map(MessageCreateEvent::getMessage)
        .filter(
            message ->
                message
                    .getAuthor()
                    .map(user -> !user.getId().equals(client.getSelfId()))
                    .orElse(false))
        .subscribe(
            message -> {
              MessageChannel channel = message.getChannel().block();
              log.debug(
                  "[{} - {}] ({}) : {}",
                  channel.getType(),
                  channel.toString(),
                  message.getAuthor().get().getUsername(),
                  message.getContent());
            });
  }
}
