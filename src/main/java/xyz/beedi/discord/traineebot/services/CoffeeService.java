package xyz.beedi.discord.traineebot.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.MessageChannel;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class CoffeeService {

  public Mono<? extends Message> sersUnCafe(MessageChannel channel, User user) {
    return sersUnTruc(ServiceTypes.CAFÉ, channel, user);
  }

  public Mono<? extends Message> sersUnThe(MessageChannel channel, User user) {
    return sersUnTruc(ServiceTypes.THÉ, channel, user);
  }

  public Mono<? extends Message> sersUnCroissant(MessageChannel channel, User user) {
    return sersUnTruc(ServiceTypes.CROISSANT, channel, user);
  }

  public Mono<? extends Message> sersUnRafraichissement(MessageChannel channel, User user) {
    return sersUnTruc(ServiceTypes.RAFRAICHISSEMENT, channel, user);
  }

  public Mono<? extends Message> sersUnTruc(ServiceTypes truc, MessageChannel channel, User user) {
    int trucPicture = new Random().nextInt(truc.getPossibilités());

    try {
      InputStream trucSream =
          new ClassPathResource(truc.getPath() + trucPicture + ".jpg").getInputStream();
      Mono<Message> toto =
          channel.createMessage(
              spec ->
                  spec.addFile(
                          StringUtils.capitalize(truc.getLabel())
                              + "pour "
                              + user.getUsername()
                              + ".jpg",
                          trucSream)
                      .setContent(
                          "Voilà votre " + truc.getLabel() + " " + user.getMention() + " !"));

      if (trucPicture == 0) {
        channel.createMessage("Oups :(").subscribe();
      }
      return toto;
    } catch (IOException e) {
      log.error("Impossible de servir le " + truc + " !", e);
      return channel.createMessage("Désolé je n'y arrive pas :(");
    }
  }
}
