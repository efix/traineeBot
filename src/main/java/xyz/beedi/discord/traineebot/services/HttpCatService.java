package xyz.beedi.discord.traineebot.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.util.Color;
import reactor.core.publisher.Mono;

@Service
public class HttpCatService {

  private static final String HTTP_CAT_ROOT_URL = "https://http.cat/";

  public String getHttpCat(HttpStatus status) {
    return HTTP_CAT_ROOT_URL + status.value() + ".jpg";
  }

  public Mono<? extends Message> afficheHttpCat(HttpStatus status, MessageChannel channel) {
    return channel.createEmbed(
        spec ->
            spec.setColor(Color.PINK)
                .setTitle(status.value() + " - " + status.getReasonPhrase())
                .setImage(getHttpCat(status))
                .setFooter("Http.Cat", null));
  }
}
