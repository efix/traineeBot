package xyz.beedi.discord.traineebot.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;

@Getter
public enum ServiceTypes {
  CAFÉ(
      5,
      "static/coffees/",
      "café",
      "☕",
      Arrays.asList("coffee", "café", "kawa", "coffees", "cafés", "kawas")),
  THÉ(5, "static/tea/", "thé", "🫖", Arrays.asList("tea", "thé", "teas", "thés")),
  CROISSANT(5, "static/croissants/", "croissant", "🥐", Arrays.asList("croissant", "croissants")),
  RAFRAICHISSEMENT(
      5,
      "static/drinks/",
      "rafraîchissement",
      "🍹",
      Arrays.asList(
          "drink", "rafraîchissement", "rafraichissement", "chaud", "soif", "a boire", "à boire"));

  private int possibilités;
  private String path;
  private String label;
  private String emoji;
  private List<String> keywords;

  private ServiceTypes(
      int possibilités, String path, String label, String emoji, List<String> keyworkds) {
    this.possibilités = possibilités;
    this.path = path;
    this.label = label;
    this.emoji = emoji;
    keywords = new ArrayList<>(keyworkds.size() + 1);
    keywords.addAll(keyworkds);
    keywords.add(emoji);
  }
}
