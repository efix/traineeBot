package xyz.beedi.discord.traineebot.services;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import discord4j.common.util.Snowflake;
import xyz.beedi.discord.traineebot.data.model.Ticket;
import xyz.beedi.discord.traineebot.data.repository.TicketRepository;

@Service
public class TicketService {

  @Autowired private TicketRepository ticketRepository;

  public List<Ticket> getAllTickets() {
    return ticketRepository.findAll();
  }

  @Transactional
  public Ticket createTicket(Snowflake user, Snowflake channel) {
    Ticket t =
        Ticket.builder()
            .user(user.asString())
            .channelId(channel.asString())
            .ticketTime(LocalDate.now())
            .treated(false)
            .build();
    ticketRepository.save(t);
    return t;
  }
}
