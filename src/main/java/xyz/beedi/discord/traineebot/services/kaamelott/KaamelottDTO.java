package xyz.beedi.discord.traineebot.services.kaamelott;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KaamelottDTO {
  private Citation citation;

  @Getter
  @Setter
  public static class Citation {
    private String citation;
    private Infos infos;
  }

  @Getter
  @Setter
  public static class Infos {
    private String personnage;
    private String saison;
    private String episode;
  }
}
