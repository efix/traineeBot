package xyz.beedi.discord.traineebot.services.kaamelott;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import lombok.Getter;
import xyz.beedi.discord.traineebot.utils.BotUtils;

@Getter
public enum KaamelottPersonnage {
  PERCEVAL,
  KADOC,
  LÉODAGAN("Leodagan", "léodagan", "roi de karmelide"),
  KARADOC,
  BOHORT,
  ARTHUR("roi"),
  ATTILA,
  GAUVAIN,
  GUENIÈVRE("guenièvre", "guenievre", "reine"),
  LANCELOT,
  LE_ROI_BURGONDE("burgonde", "Le Roi Burgonde"),
  MERLIN,
  LOTH,
  DAGONET,
  VENEC,
  BELT,
  GOUSTAN,
  Grüdü("Grudu"),
  GUETHENOC,
  MEVANWI,
  ROPARZH,
  LE_TAVERNIER("tavernier"),
  SÉLI("Séli", "seli"),
  YVAIN;

  List<String> keyWorkds = new ArrayList<>(Arrays.asList(name()));
  Pattern pattern;
  String label;

  private KaamelottPersonnage(String... otherKeywords) {

    keyWorkds.addAll(Arrays.asList(otherKeywords));
    pattern = BotUtils.patternForKeywords(keyWorkds);
    label = StringUtils.capitalize(name().toLowerCase().replace('_', ' '));
  }

  private KaamelottPersonnage() {
    pattern = BotUtils.patternForKeywords(keyWorkds);
    label = StringUtils.capitalize(name().toLowerCase().replace('_', ' '));
  }

  public static KaamelottPersonnage find(String personnage) {
    for (KaamelottPersonnage perso : values()) {
      if (perso.getPattern().matcher(personnage).matches()) {
        return perso;
      }
    }
    return null;
  }
}
