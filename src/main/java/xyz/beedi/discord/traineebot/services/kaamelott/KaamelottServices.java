package xyz.beedi.discord.traineebot.services.kaamelott;

import java.util.function.Consumer;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import reactor.core.publisher.Mono;
import xyz.beedi.discord.traineebot.services.kaamelott.KaamelottDTO.Citation;

@Service
public class KaamelottServices {
  private static final String ROOT_API_URL = "https://kaamelott.chaudie.re/api";

  private RestTemplate restTemplate = new RestTemplate();

  public Citation citationAuPif() {
    return restTemplate
        .getForEntity(ROOT_API_URL + "/random", KaamelottDTO.class)
        .getBody()
        .getCitation();
  }

  public Citation citerUnPersonnage(KaamelottPersonnage personnage) {
    return restTemplate
        .getForEntity(
            ROOT_API_URL
                + "/random/personnage/"
                + personnage.name().toLowerCase().replaceAll("_", " "),
            KaamelottDTO.class)
        .getBody()
        .getCitation();
  }

  public Mono<? extends Message> citerUnPersonnage(
      KaamelottPersonnage personnage, MessageChannel channel) {
    Citation citation = citerUnPersonnage(personnage);
    return channel.createEmbed(genereCitation(citation));
  }

  public Mono<? extends Message> citerUnPersonnageAuPif(MessageChannel channel) {
    Citation citation = citationAuPif();
    return channel.createEmbed(genereCitation(citation));
  }

  private Consumer<? super EmbedCreateSpec> genereCitation(Citation citation) {
    return spec ->
        spec.setColor(Color.DARK_GOLDENROD)
            .setTitle(" - " + citation.getInfos().getPersonnage() + " - ")
            .setAuthor(
                "Kaamelott",
                null,
                "https://static.wikia.nocookie.net/kaamelott-officiel/images/9/91/Kaamelott_logo.png/revision/latest?cb=20150102043508&path-prefix=fr")
            .setDescription(citation.getCitation())
            .setFooter(
                citation.getInfos().getSaison() + " - " + citation.getInfos().getEpisode(), null);
  }
}
