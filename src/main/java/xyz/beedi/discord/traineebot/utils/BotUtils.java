package xyz.beedi.discord.traineebot.utils;

import java.util.List;
import java.util.regex.Pattern;

public class BotUtils {

  public static Pattern patternForKeywords(List<String> keywords) {
      return Pattern.compile(
          "(.*[^\\w])?(" + String.join("|", keywords) + ")([^\\w].*)?", Pattern.CASE_INSENSITIVE);
    }}
